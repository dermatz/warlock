# CHANGELOG
All notable changes in this project are documented in this file.

[![Dermatz on Twitter](https://img.shields.io/twitter/follow/_dermatz.svg?style=social&label=Follow%20@_dermatz)](https://twitter.com/_dermatz/)


## Latest Release

### [1.0.0](https://gitlab.com/dermatz/warlock/-/releases)
>- Add NPX Installation Script
