/**
 * WARLOCK FRONTEND BUILDTOOL
 * BASED ON WEBPACK & LARAVEL-MIX
 * AUTHOR: Mathias Elle
 * URL: https://www.dermatz.de
 * GITHUB: https://github.com/dermatz/warlock
 * ==================================================
 * Report Issues here: https://github.com/dermatz/warlock/issues
 * Add new Pull Requests here: https://github.com/dermatz/warlock/pulls
 */

'use strict';



// Warlock variables
const warlock = require( './warlock.config.js' ); // warlock config file
const warlockVersion = require(warlock.corepath + 'package.json');

// Laravel Mix core
const mix = require('laravel-mix');
require('laravel-mix-criticalcss');
require('laravel-mix-eslint');
require('laravel-mix-imagemin');

log('You are using Warlock Version ' + packageinfo.version);




    // JavaScript Files  =========================================================================================================
mix.js('src/app.js', 'dist/');

    // SCSS Files  ===============================================================================================================
mix.sass('src/app.scss', 'dist/');

    // BrowserSync  ==============================================================================================================
if(warlock.browsersync_support === true) {
    mix.browserSync({
        proxy: warlock.browsersync_proxyUrl,
        port: warlock.browsersync_port,
        ui: warlock.browsersync_portUI,
        https: warlock.browsersync_https,
        notify: warlock.browsersync_notify,
        ghostMode: warlock.browsersync_ghostmode,
        open: warlock.browsersync_new_tab
    }, browserSyncReuseTab);
}

// mix.combine(files, destination);
mix.babel(files, destination); // <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
mix.minify(file);
mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
