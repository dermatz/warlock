#!/usr/bin/env node
'use strict';
const fs = require('fs');
const ora = require('ora');
const execa = require('execa');
const chalk = require('chalk');
const download = require('download');

const handleError = require('./handleError.js');
const clearConsole = require('./clearConsole.js');
const printNextSteps = require('./printNextSteps.js');
const theCWD = process.cwd() + '/warlock';
const core = process.cwd() + '/warlock/core';
const theCWDArray = theCWD.split('/');
const theDir = theCWDArray[theCWDArray.length - 1];

module.exports = () => {
    clearConsole();
    const filesToDownload = [
        'https://raw.githubusercontent.com/dermatz/warlock/master/package.json',
        'https://raw.githubusercontent.com/dermatz/warlock/master/LICENSE',
        'https://raw.githubusercontent.com/dermatz/warlock/master/README.md',
        'https://raw.githubusercontent.com/dermatz/warlock/master/CHANGELOG.md',
        'https://raw.githubusercontent.com/dermatz/warlock/master/core/.eslintrc.js',
        'https://raw.githubusercontent.com/dermatz/warlock/master/core/webpack.mix.js',
        'https://raw.githubusercontent.com/dermatz/warlock/master/core/warlock.config.js',
    ];

    // include hidden Dotfiles
    const dotFiles = ['.eslintrc.js'];

    // Install Process ...
    console.log('\n');
    console.log(
        '📦 ',
        chalk.black.bgBlueBright(` Downloading Warlock source in: → ${chalk.black.bgYellowBright(` ${theDir} `)}\n`),
        chalk.dim(`\n The current install directory is: ${chalk.yellow(theCWD)}\n`),
        chalk.dim('This might take a moment...\n')
    );

    const spinner = ora({text: ''});
    spinner.start(`1. Downloading Warlock source in → ${chalk.black.bgGreenBright(` ${theDir} `)}`);

    // Download
    Promise.all(filesToDownload.map(x => download(x, `${core}`))).then(async () => {
        dotFiles.map(x => fs.rename(`${core}/${x.slice(1)}`, `${core}/${x}`, err => handleError(err)));
        spinner.succeed();

        const packages = new Promise(function (resolve, reject) {
            spinner.start('2. Check if all Warlock files are ready ...');
            setTimeout(function () {
                fs.access(theCWD +'/package.json', fs.F_OK, (notexist) => {
                    if (notexist) {
                        fs.copyFile( core + '/package.json', theCWD + '/package.json', () => {});
                        spinner.succeed(`2. Packages are installed successfully`);
                        resolve('Packages downloadet');
                    } else {
                        spinner.succeed(`2. Core-Dependencies already downloaded.`);
                        resolve('Packages renewed');
                    }
                });
            }, 3000);
        });

        await packages.then(function whenOk(response) {
            return response;
        });

        // The npm install.
        spinner.start('3. Install Warlock dependencies ... please wait ...');
        await execa('npm', ['--prefix', theCWD, 'install', theCWD]);
        spinner.succeed();

        // CHECK WEBPACK.MIX.JS
        const check_webpack_mix = new Promise(function (resolve, reject) {
            spinner.start('4. Check webpack.mix.js ...');
            setTimeout(function () {
                fs.access(theCWD + '/webpack.mix.js', fs.F_OK, (notFound) => {
                    if (notFound) {
                        fs.rename(theCWD + '/core/webpack.mix.js', theCWD + '/webpack.mix.js', function () {
                        });
                        spinner.succeed(`4. All right. The ${chalk.yellow('webpack.mix.js')} is ready now.`);
                        resolve('renamed');
                    } else {
                        spinner.succeed(`4. The ${chalk.yellow('webpack.mix.js')} is already there!`);
                        resolve('found');
                    }
                });
            }, 2000);
        });

        await check_webpack_mix.then(function whenOk(response) {
            return response;
        });

        // CHECK WARLOCK CONFIG
        const check_warlock_config = new Promise(function (resolve, reject) {
            spinner.start('5. Check the new WEBPACK Config file ...');
            setTimeout(function () {
                fs.access(theCWD + '/warlock.config.js', fs.F_OK, (notFound) => {
                    if (notFound) {
                        fs.copyFile(core + '/warlock.config.js', theCWD + '/warlock.config.js', () => {});
                        spinner.succeed(`5. Yeah. The new s${chalk.yellow('warlock.config.js')} is ready now.`);
                        resolve('renamed');
                    } else {
                        spinner.succeed(`5. The ${chalk.yellow('warlock.config.js')} found!`);
                        resolve('found');
                    }
                });
            }, 2000);
        });

        await check_warlock_config.then(function whenOk(response) {
            return response;
        });

        printNextSteps();
    });
}
