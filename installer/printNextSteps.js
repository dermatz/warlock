const chalk = require('chalk');

module.exports = () => {
console.log('\n\n✅ ', chalk.black.bgGreen(' Installation complete! 🦄  Make the web a better place. \n'));
console.log('\n✊ ', chalk.black.bgYellow(' Support Warlock \n'));
console.log(
    chalk.white.bold('Do you like Warlock?'),
    '\nYou can now support this open source project: \n'
);
console.log(
    `	${chalk.yellow('Support with coffee (via PayPal) →')} paypal.me/dermatz`,
    '\n',
    `	${chalk.yellow('Report an issue →')} https://github.com/dermatz/warlock/issues`,
    '\n',
    `	${chalk.yellow('Become a contributor →')} https://github.com/dermatz/warlock/pulls`
);

console.log('\n\n🎯 ', chalk.black.bgGreen(' Getting Started...\n'));
console.log(' Next Steps → \n');
console.log(` ${chalk.dim('👉')} ${chalk.green('cd warlock')} and ..`);
console.log(` ${chalk.dim('1.')} Editing the new ${chalk.green('warlock.config.js')} file to specify your theme structure.`);
console.log(` ${chalk.dim('3.')} Run ${chalk.green('npm .... ')} to complete the Warlock installation.`, '\n');
console.log('\n\n🎯 ', chalk.black.bgGreen(' Updates ...\n'));
console.log('📦 ', ` If you want to update warlock in the future, just run ${chalk.green('npm run warlock-update')} to get the newest version.`, '\n\n');
};
